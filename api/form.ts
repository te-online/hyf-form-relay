import { VercelRequest, VercelResponse } from "@vercel/node";
import sanitizeHtml from "sanitize-html";

export default function handler(req: VercelRequest, res: VercelResponse) {
  const generatePage = (formData) => `<html>
  <head>
    <title>HYF Form Result</title>
    <style>
      * {
        font-family: -apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, Helvetica, Arial, sans-serif, "Apple Color Emoji", "Segoe UI Emoji", "Segoe UI Symbol";
      }
    </style>
  </head>
  <body>
    <h1>You sent this information in your form 🤡</h1>
    <p>Used method: ${req.method}</p>
    <p>Server url: ${req.url}</p>
    <pre>${sanitizeHtml(JSON.stringify(formData, null, 2), {
      allowedTags: [],
      disallowedTagsMode: "escape",
    })}</pre>
  </body>
</html>`;
  if (req.method === "POST") {
    res.send(generatePage(req.body));
  } else if (req.method === "GET") {
    res.send(generatePage(req.query));
  } else {
    res.send("🤡");
  }
}
